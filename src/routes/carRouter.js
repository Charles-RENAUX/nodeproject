const express = require("express");
const router = express.Router()
const carController = require('../controller/carController')
const {myOwnMiddleware} = require('../middleware/carMiddleware')

/*----MIDDLEWARE----*/
router.use(myOwnMiddleware)

/*----ROUTES----*/
router.route('/')
    .get(carController.findAll)
    .post(carController.create)

router.route('/:id')
    .get(carController.findById)
    .patch(carController.findByIdAndUpdate)
    .delete(carController.findByIdAndDelete)

module.exports = router;