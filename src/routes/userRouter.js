const express = require("express");
const router = express.Router()
const userController = require('../controller/userController')
const {myOwnMiddleware} = require('../middleware/userMiddleware')

router.use(myOwnMiddleware)
// routes
router.route('/')
    .get(userController.findAll)
    .post(userController.create)

router.route('/:id')
    .get(userController.findById)
    .patch(userController.findByIdAndUpdate)
    .delete(userController.findByIdAndDelete)

module.exports = router;