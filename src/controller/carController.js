const logger = require('../utils/logger')
const Car = require('../models/carModel')
const QueryBuilder = require('../utils/queryBuilder')

const findAll = async (req,res,next) => {
        logger.info(req.query)
        try{
            let query = new QueryBuilder(Car.find(),req.query).filder()
            let document = await query.mongoQuery
            //const document = await Car.find()
            res.status(200).json({
                data : document
            })
        }catch(error){
            logger.error(error)
            res.status(500).json({
                description: "An error occured in our part, sorry",
                error: error.stack
            })
        }
}

const findById = async (req,res,next) => {
    try{
        const document = await Car.findById(req.params.id)
        if (document) {
        res.status(200).json({
            data: document
        })
        } else {
            res.status(204).json({
                description: `user not found`
            })
        }
    }catch(error){
        logger.error(error)
        res.status(500).json({
            description: "An error occured in our part, sorry",
            error: error.stack
        })
    }
}

const findByIdAndDelete = async (req,res,next)=>{
    try{
        const document = await Car.findByIdAndDelete(req.params.id)
        if (document) {
            res.status(200).json({
                data: document
            })
        }
    }catch(error){
        logger.error(error)
        res.status(500).json({
            description: "An error occured in our part, sorry",
            error: error.stack
        })
    }
}

const findByIdAndUpdate = async (req,res,next)=>{
    try {
        const document = await Car.findByIdAndUpdate(req.params.id,req.body)
        if (document) {
            res.status(200).json({
                old: document,
                new : req.body
            })
        }
    } catch(error){
        logger.error(error)
        res.status(500).json({
            description: "An error occured in our part, sorry",
            error: error.stack
        })
    }
    
}

const create = async (req,res,next)=>{
    try {
        logger.info(req.body)
        if (req.body) {
            const user = await Car.create(req.body);
            res.status(200).json({
                data: user
            })
        }
    } catch(error){
        logger.error(error)
        res.status(500).json({
            description: "An error occured in our part, sorry",
            error: error.stack
        })
    }
   
}

module.exports = {
    findAll:findAll,
    findById:findById,
    findByIdAndDelete:findByIdAndDelete,
    findByIdAndUpdate:findByIdAndUpdate,
    create:create
}