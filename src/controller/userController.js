const logger = require('../utils/logger')
const User = require('../models/userModel')
const AppError = require('../utils/errorHandler')
const match = require('nodemon/lib/monitor/match')
const config = require('../../config')
const QueryBuilder = require('../utils/queryBuilder')

const findAll = async (req,res,next) => {
        logger.info(req.query) 
        try{
            /*
            // sort (value)  sort(value,value) -> trie
            if (sort){
                sort = req.query.sort.split(',').join(' ')
            }
            // fields (value) fields(value,value) -> selection
            if (fields){
                fields = req.query.fields.split(',').join(' ')
            }

            // page 2 limit 10
            page = page*1 || 1
            // skip = limit*(page-1)
            limit = limit*1|| config.limitUser
            // mogoose -> skip : skip element
            const skip = limit*(page-1)
            // mongoose -> limit : take a limit of elements    
            */

            let query = new QueryBuilder(User.find(),req.query).filder()
            //const document = await User.find(JSON.parse(query)).sort(sort).select(fields).skip(skip).limit(limit)
            let document = await query.mongoQuery
            res.status(200).json({
                data : document
            })
        }catch(error){
            logger.error(error)
            res.status(500).json({
                description: "An error occured in our part, sorry",
                error: error.stack
            })
        }
}

const findById = async (req,res,next) => {
    try{
        const document = await User.findById(req.params.id)
        if (!document) {
            return next(new AppError('User not found',404))
        } 
        res.status(200).json({
            data: document
        })
    }catch(error){
        next(new AppError('An error occured',500))
    }
}

const findByIdAndDelete = async (req,res,next)=>{
    try{
        const document = await User.findByIdAndDelete(req.params.id)
        if (document) {
            res.status(200).json({
                data: document
            })
        }
    }catch(error){
        logger.error(error)
        res.status(500).json({
            description: "An error occured in our part, sorry",
            error: error.stack
        })
    }
}

const findByIdAndUpdate = async (req,res,next)=>{
    try {
        const document = await User.findByIdAndUpdate(req.params.id,req.body)
        if (document) {
            res.status(200).json({
                old: document,
                new : req.body
            })
        }
    } catch(error){
        logger.error(error)
        res.status(500).json({
            description: "An error occured in our part, sorry",
            error: error.stack
        })
    }
    
}

const create = async (req,res,next)=>{
    try {
        logger.info(req.body)
        if (req.body) {
            const user = await User.create(req.body);
            res.status(200).json({
                data: user
            })
        }
    } catch(error){
        logger.error(error)
        res.status(500).json({
            description: "An error occured in our part, sorry",
            error: error.stack
        })
    }
   
}

module.exports = {
    findAll:findAll,
    findById:findById,
    findByIdAndDelete:findByIdAndDelete,
    findByIdAndUpdate:findByIdAndUpdate,
    create:create
}