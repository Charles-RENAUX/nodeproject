const express = require("express");
const router = express.Router()
const mongoose = require('mongoose');
const { required } = require("nodemon/lib/config");
const validator = require('validator')

/*---- CARS SCHEMAS---- */
const carSchema = new mongoose.Schema({
    brand:{
        type:String,
        required :[true, 'a car adress must have a brand']
    },
    year: {
        type: Number
    },
    color: {
        type:String,
        trim:true,
        lowercase:true
    },
    category : {
        type: String,
        enum: ['4X4', 'SUV', 'sport', 'break', 'city car'],
        required :[true, 'a car adress must have a category']
    },
    fuel : {
        type:String,
        enum: ['diesel', 'essence', 'electrical', 'hybrid'],
        required :[true, 'a car adress must have a fuel']
    },
})

/*---- END SCHEMAS---- */

// the instance model need the name of the model and the schema
const Car = mongoose.model('Car', carSchema)
module.exports=Car;