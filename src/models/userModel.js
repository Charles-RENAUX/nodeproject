const express = require("express");
const router = express.Router()
const mongoose = require('mongoose');
const { required } = require("nodemon/lib/config");
const validator = require('validator')

// schemas
const userSchema = new mongoose.Schema({
    lastname: {
      type: String,
      trim: true,
    },
    firstname: {
      type: String,
      required: [true, 'a user must have a firstname'],
      trim: true,
    },
    dateOfBirth:{
      type: Date,
      required: [true, 'a user must have a date of birth'],
    },
    createAt:{
      type: Date,
      default: Date.now,
    },
    email:{
      type: String,
      required: [true, 'a user must have an email'],
      trim: true,
      lowercase: true,
      validate: validator.isEmail,
      unique: true
    },
    //adress:{
    adress: {
      type: String,
      required :[true, 'a user adress must have an explicite adress']
      // embeded data
    },
    /*
      zipCode: Number,
      city: String,
      coordinates: [Number],
    },*/
    rentedCar : {
      type: mongoose.Schema.ObjectId,
    },
    ownedCars :[{
      type: mongoose.Schema.ObjectId,
    }],
    rating : {
      type: Number,
      default: 90,
      max:100,
      min:0
    },
    pastContracts : {
      type: Number,
      default: 0,
      min:0
    },
    level: {
      type: String,
      enum:['noob','human','god'],
      defaults: 'noob' 
    },
   })

// the instance model need the name of the model and the schema
const User = mongoose.model('User', userSchema)

module.exports=User;