const config = require('../../config')
class QueryBuilder{
    constructor(mongoQuery,query){
        this.mongoQuery=mongoQuery;
        this.query=query
    }

    filder(){
        let {sort, fields, page, limit, ...filters}= this.query
        // --filters
        filters = JSON.stringify(filters)
        filters = filters.replace(/\b(gte|gt|lte|lt)\b/g,match => `$${match}`);
        
        // sort (value)  sort(value,value) -> trie
        if (sort){
            sort = sort.split(',').join(' ')
            console.log("🚀 ~ file: queryBuilder.js ~ line 15 ~ QueryBuilder ~ filder ~ sort", sort)
        }
        // fields (value) fields(value,value) -> selection
        if (fields){
            fields = fields.split(',').join(' ')
        }
        // page 2 limit 10
        page = page*1 || 1
        // skip = limit*(page-1)
        limit = limit*1|| config.limitUser
        // mogoose -> skip : skip element
        const skip = limit*(page-1)

        this.mongoQuery.find(JSON.parse(filters)).sort(sort).select(fields).skip(skip).limit(limit)
        

        return (this)
    }

}


module.exports =  QueryBuilder
