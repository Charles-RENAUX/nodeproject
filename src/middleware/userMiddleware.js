const { schema } = require("../models/userModel")
const logger = require('../utils/logger')

myOwnMiddleware = (req,res,next) => {
    logger.info('--------------USER--------------')
    next()
}
schema.pre('save',function(next){
    logger.info(this)
    next()
})
module.exports={
    myOwnMiddleware,
}