const express = require('express');
const app = express();
const morgan = require('morgan');
const userRoute = require('./routes/userRouter')
const carRoute = require('./routes/carRouter')
const logger = require('./utils/logger')

app.use(express.json())
app.use(morgan('dev'))

//routes
app.use('/users',userRoute)
app.use('/cars',carRoute)

app.use('*',(req,res,next)=>{
    res.status(400).json({
        description:'this route does not exist for the moment but might be available in the future'
    })
})

app.use((err,req,res,next)=> {
    if(process.env.NODE_ENV==='development'){
        logger.error(err)
        res.status(err.statusCode).json({
        message : err.message,
        errorStack : err.stack
    })}
}) 

module.exports = app