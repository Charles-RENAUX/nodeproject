const app = require('./src/app');
const mongoose = require('mongoose');
const config = require('./config')
const logger = require('./src/utils/logger')

//console.log(config)
//logger.error('ERROR')
//logger.warn("WARN")
//logger.info("INFO ")
//logger.debug("DEBUG")

mongoose
.connect(`mongodb://${config.urlMongoDB}/${config.databaseMongoDB}`)
.then(() => logger.warn('DB connection successful!'))

app.listen(config.port, () => {
    logger.info('listen to port : '+config.port)
})
