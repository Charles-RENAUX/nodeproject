const mongoose = require('mongoose');
const User = require('../src/models/userModel')
const config = require('../config')
let users = require("./users2.json");
let cars = require("./car.json")
const Car = require('../src/models/carModel');

(async ()=>{
    try {
        await mongoose.connect(`mongodb://${config.urlMongoDB}/${config.databaseMongoDB}`)
        console.log('DB connection successful!')
        await Promise.all([User.deleteMany({}), Car.deleteMany({})])
        await Promise.all([User.create(users),Car.create(cars)])
        mongoose.disconnect()
    } catch (error) {
        console.log("🚀 ~ file: updateDatabase.js ~ line 39 ~ error", error)  
    }
    
})()
