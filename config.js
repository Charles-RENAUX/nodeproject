const dotenv = require('dotenv')
dotenv.config()
// add newpath .env ==> dotenv.config().path()

module.exports = {
    port : process.env.PORT,
    usernameMongoDB: process.env.USERNAME_MONGODB,
    passwordMongoDB: process.env.PASSWORD_MONGODB,
    urlMongoDB: process.env.URL_MONGODB,
    databaseMongoDB: process.env.DATABASE_MONGODB,
    limitUser: process.env.LIMIT_USER
}